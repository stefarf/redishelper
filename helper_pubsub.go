package redishelper

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/stefarf/iferr"
)

func (rh *Helper) SubscribeAndConsume(channels []string, consume func(channel, payload string) error) error {
	var redisChannels []string
	for _, ch := range channels {
		redisChannels = append(redisChannels, fmt.Sprintf("%s:%s", rh.namespace, ch))
	}

	ps := rh.cli.Subscribe(ctx, redisChannels...)
	defer func() { iferr.Print(ps.Close()) }()

	for {
		msg, err := ps.ReceiveMessage(ctx)
		if err != nil {
			return err
		}

		err = consume(strings.TrimPrefix(msg.Channel, rh.namespace+":"), msg.Payload)
		if err != nil {
			return err
		}
	}
}

func (rh *Helper) PublishString(channel string, message string) error {
	redisChannel := fmt.Sprintf("%s:%s", rh.namespace, channel)
	return rh.cli.Publish(ctx, redisChannel, message).Err()
}

func (rh *Helper) PublishMarshal(channel string, message any) error {
	b, err := json.Marshal(message)
	if err != nil {
		return err
	}
	redisChannel := fmt.Sprintf("%s:%s", rh.namespace, channel)
	return rh.cli.Publish(ctx, redisChannel, b).Err()
}
