package redishelper

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/stefarf/iferr"
)

func (rh *Helper) Del(key string) error {
	redisKey := fmt.Sprintf("%s:%s", rh.namespace, key)
	return rh.cli.Del(ctx, redisKey).Err()
}

func (rh *Helper) SetMarshal(key string, value any, expiration time.Duration) error {
	b, err := json.Marshal(value)
	iferr.Panic(err)
	redisKey := fmt.Sprintf("%s:%s", rh.namespace, key)
	return rh.cli.Set(ctx, redisKey, b, expiration).Err()
}

func (rh *Helper) SetString(key string, value string, expiration time.Duration) error {
	redisKey := fmt.Sprintf("%s:%s", rh.namespace, key)
	return rh.cli.Set(ctx, redisKey, value, expiration).Err()
}

func (rh *Helper) GetUnmarshal(key string, value any) error {
	redisKey := fmt.Sprintf("%s:%s", rh.namespace, key)
	rst := rh.cli.Get(ctx, redisKey)
	b, err := rst.Bytes()
	if err != nil {
		return err
	}
	return json.Unmarshal(b, value)
}

func (rh *Helper) GetString(key string) (string, error) {
	redisKey := fmt.Sprintf("%s:%s", rh.namespace, key)
	rst := rh.cli.Get(ctx, redisKey)
	b, err := rst.Bytes()
	if err != nil {
		return "", err
	}
	return string(b), nil
}
