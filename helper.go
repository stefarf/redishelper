package redishelper

import (
	"context"

	"github.com/redis/go-redis/v9"
)

var ctx = context.Background()

type Helper struct {
	cli       *redis.Client
	namespace string
}

func New(redisServer, namespace string) *Helper {
	return &Helper{
		cli:       redis.NewClient(&redis.Options{Addr: redisServer}),
		namespace: namespace,
	}
}
