package redishelper

import (
	"log"
)

var Instance *Helper

func Init(redisServer, namespace string) {
	if Instance == nil {
		Instance = New(redisServer, namespace)
		log.Println("Redis client initialized")
	}
}
